**CeSViMa Docs**

| A finales del año 2004, la Universidad Politécnica de Madrid aunó esfuerzos con IBM y el Centro Nacional de Supercomputación para crear el Centro de Supercomputación y Visualización  de  Madrid  (CeSViMa).  El  CeSViMa,  situado  el  Campus  de  Excelencia Internacional de Montegancedo una de las sedes del Parque Científico-Tecnológico de la UPM, se centra en el almacenamiento masivo de información, computación de altas prestaciones, visualización  interactiva  avanzada,  virtualización  y  computación  en  la nube. Dentro  del  área  de  computación  de  altas  prestaciones  oferta  recursos  mediante  el supercomputador Magerit. La segunda versión de este supercomputador se convirtió en el  más  potente  de  España  en  la  lista  TOP500  de  junio  de  2011. También  se  posicionó como el más ecológico de España y uno de los más ecológicos del planeta



Bienvenido a la página de documentación del Centro de Supercomputación y Visualización de Madrid.
Aquí podrás consultar información sobre cualquier servicio o utilidad de los que ofrecemos.

Manuales
~~~~~~~~

`Manual de computación de altas prestaciones (HPC)`_

.. _Manual de computación de altas prestaciones (HPC) : http://doc-dev.cesvima.upm.es/docs/portada/projects/hpc/es/latest/manualHPC/index.html

`Manual de UPM Drive`_

.. _Manual de UPM Drive: http://doc-dev.cesvima.upm.es/docs/portada/projects/upmdrive/es/latest/manualUPMdrive/index.html

`Manual de VPS`_

.. _Manual de VPS : http://doc-dev.cesvima.upm.es/docs/portada/projects/vps/es/latest/manualVPS/index.html

`Manual de HPC(2)`_

.. _Manual de HPC(2) : http://doc-dev.cesvima.upm.es/docs/portada/projects/slurm/es/latest/manualSlurm/index.html
