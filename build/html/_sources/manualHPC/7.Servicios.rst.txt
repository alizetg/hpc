************
7. Servicios
************

Además de los recursos de computación, el CeSViMa proporciona una serie de servicios auxiliares a todos los usuarios de sus recursos.

Prácticamente la totalidad de los servicios son activados en el momento que se abre la correspondiente cuenta en el sistema y permanecerán en ese estado hasta el cierre de la misma. Algunos servicios pueden deshabilitarse bajo petición expresa del usuario.

7.1 Centro de atención a usuarios
#################################

Para solucionar cualquier duda o problema que surja durante el uso de los recursos ofertados se puede contactar con el Centro de Atención a Usuarios[#f1]_ . El servicio de atención a usuarios únicamente se presta por correo electrónico en horario de oficina.

En la consulta se debe de indicar siempre que sea posible y de la forma más clara:

* Descripción de la duda o problema surgido.

* Identificación del usuario (login) y proyecto asociado, sobre todo si se utiliza una cuenta de correo diferente a la utilizada en el registro.

* Día, fecha y hora aproximados en la que se detectó la incidencia.

* Condiciones en las que se produce.

* Mensajes de error o información mostrados por la máquina en el momento de la incidencia, si se dispone de ellos.

* Si la incidencia es sobre algún trabajo que ejecuta en la máquina, toda la información relativa al trabajo que ocasiona el problema: identificador de trabajo, aplicación ejecutada, mensajes de error, ficheros de log, etc.

Cada una de las comunicaciones iniciadas queda automáticamente registrada en un sistema de control de incidencias asignándoles un identificador único y conservándose su histórico.
.. [#f1] support@cesvima.upm.es

7.2 Listas de distribución
##########################

Todas las direcciones de correo de usuarios y líderes de proyecto son añadidas de forma automática a las listas de distribución del Centro. Estas listas son utilizadas para proporcionar información a todos los usuarios: incidencias de servicio, invitaciones a conferencias o seminarios, etc.

Para evitar el correo no deseado, la lista está cerrada de tal forma que únicamente el personal de soporte del CeSViMa puede enviar correos a las listas.

Asimismo, al finalizar la relación entre el usuario y el CeSViMa la dirección del usuario es eliminada de la lista.

7.3 Instalación de software
###########################

Además de las aplicaciones básicas incluidas en el sistema operativo, Magerit dispone de un repositorio de aplicaciones en /sw. En el repositorio existen versiones de múltiples aplicaciones adaptadas a su funcionamiento en el sistema.

Si se necesita una versión de alguna aplicación no disponible en el repositorio o una actualización a una versión más moderna se puede solicitar contactando con el Centro de Atención a Usuarios.

Asimismo, si el software a emplear precisa licencia es necesario enviar al Centro de Atención a Usuarios una copia de la misma. Este software tendrá protegido el acceso y sólo podrán acceder aquellos grupos o usuarios que acrediten tener una licencia válida.

