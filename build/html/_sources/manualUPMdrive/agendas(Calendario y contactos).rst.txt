3. Agendas (Calendario y contactos)
===================================

Además del almacenamiento UPMdrive proporciona calendarios y agenda de contactos.
Es posible acceder a estas funciones desde el menú superior (situado al lado del logo) de la inter-faz web.
Tanto los calendarios como los contactos utilizan los protocolos estándar CalDAV y CardDAV, respectivamente, lo que permite que numerosas aplicaciones y dispositivos puedan acceder a ellos.

3.1 Compartir calendarios o contactos
*************************************

Al igual que ficheros es posible compartir calendarios o contactos entre usuarios de UPMdrive. Desde la aplicación adecuada (seleccionada en el menú superior al lado del logo).

1.	Pulsar el símbolo de compartir situado al lado de cada calendario
2.	Indicar el usuario con el que se desea compartir

Se debe escribir la cuenta UPM de la persona con la que se desea compartir y esperar unos segundos a que aparezca el nombre completo del usuario.

.. image:: ../../html/images/img25.jpg

.. Para notas
.. topic:: Nota:

	El nombre de usuario es la parte izquierda del correo UPM (habitualmente, nom-bre.apellido) sin el @upm.es

3. Indicar los permisos

.. image:: ../../html/images/img26.jpg

3.2 Configurar clientes
***********************

Al utilizar los protocolos estándar CalDAV y CardDAV es posible acceder a los calendarios y contactos utilizando clientes conocidos. En algunos casos, la integración es nativa, mientras que en otros es necesario instalar y configurar alguna pequeña extensión, plugin o aplicación que ha-ga de traductor entre el cliente y el servidor.
Aunque se describe cómo se configuran algunos de los más utilizados, los pasos deberían ser equivalentes en cualquier otro cliente.


3.2.1	Microsoft Outlook
*************************

Para poder utilizar los calendarios y contactos de UPMdrive en Microsoft Outlook es necesario instalar y configurar una extensión
1.	Instalar el plugin Outlook CalDav Synchronizer  con Microsoft Outlook cerrado.
2.	Abrir Microsoft Outlook
3.	Ir a calendario
4.	Abrir la nueva pestaña CalDAV Synchronizer

.. image:: ../../html/images/img27.jpg

5.	Abrir Synchronization Profiles
6.	Seleccionar «Add a new profile»
7.	Elegir NextCloud

.. image:: ../../html/images/img28.jpg

8.	Rellenar los datos de conexión a UPMdrive:

.. image:: ../../html/images/img29.jpg

.. image:: ../../html/images/img30.jpg

9.	Seleccionar «Test or Discover settings».
10.	En el listado de calendarios disponible en UPMdrive elegir uno de ellos

.. image:: ../../html/images/img31.jpg

11.	Elegir la dirección e intervalo de sincronización
12.	Confirmar con «Synchronize Now»

3.2.2	Mozilla Thunderbird
***************************
Mozilla Thunderbird dispone de dos extensiones para poder acceder a los calendarios (Ligthing) y contactos (Sogo connector).

3.2.2.1	Calendarios
*******************

Mozilla Thunberdird tiene una extensión denominada Lighting capaz de sincronizar los calenda-rios. Esta extensión sólo permite añadir un calendario individual por lo que los pasos hay que repetirlos para cada calendario que se desee añadir.

1.	Abrir el cliente de calendarios en Mozilla Thunderbird

.. image:: ../../html/images/img32.jpg

2.	Abrir el menú contextual y seleccionar nuevo calendario

.. image:: ../../html/images/img33.jpg

3.	Seleccionar un calendario CalDAV

.. image:: ../../html/images/img34.jpg

4.	Indicar la dirección del calendario

Esta dirección se debe obtener desde la interfaz web y tendrá el formato:
https://drive.upm.es/remote.php/dav/calendars/[nombre.apellido]/[calendario]/

.. image:: ../../html/images/img35.jpg

3.2.3	Dispositivos Android
****************************

Para poder acceder al calendario y los contactos desde un dispositivo Android, es necesario insta-lar algún cliente que proporcione el soporte de los protocolos.
Aunque existen numerosas opciones, se van a describir dos. En primer lugar, la aplicación Dav Droid:

1.	Instalar la aplicación Dav Droid desde la Play Store.

.. image:: ../../html/images/img36.jpg
   :scale: 30 %

.. image:: ../../html/images/img37.jpg
   :scale: 30 %

.. image:: ../../html/images/img38.jpg
   :scale: 30 %

.. image:: ../../html/images/img39.jpg
   :scale: 30 %

.. image:: ../../html/images/img40.jpg
   :scale: 30 %

.. image:: ../../html/images/img41.jpg
   :scale: 30 %

2.	Abrir la aplicación


La primera vez que se abre aparecerán dos diálogos: uno indicando la licencia y otro indi-cando una alternativa para sincronizar tareas (recomendamos seleccionar «Don’t show again».

.. image:: ../../html/images/img42.jpg
   :scale: 50 %

.. image:: ../../html/images/img43.jpg
   :scale: 50 %

3.	Añadir una nueva cuenta, pulsando en el botón + situado abajo a la derecha.
4.	Elegir «Login with URL and username».

.. image:: ../../html/images/img44.jpg
   :scale: 50 %

.. image:: ../../html/images/img45.jpg
   :scale: 50 %

5.	Indicar como dirección https://drive.upm.es
6.	Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios.

.. image:: ../../html/images/img46.jpg
   :scale: 50 %

7.	Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdri-ve)
8.	Elegir el nombre de la cuenta y confirmar seleccionando «Create Account»

.. image:: ../../html/images/img47.jpg
   :scale: 50 %

9.	En la pantalla de inicio, aparecerá la cuenta creada.

.. image:: ../../html/images/img48.jpg
   :scale: 50 %

10.	Seleccionar la cuenta
11.	Elegir los calendarios y contactos se quieren sincronizar.
12.	Forzar una sincronización seleccionando el icono de refrescar en la parte superior dere-cha

.. image:: ../../html/images/img49.jpg
   :scale: 50 %

.. image:: ../../html/images/img50.jpg
   :scale: 50 %

A partir de este momento, en el calendario y contactos aparecerán los de UPMdrive

.. image:: ../../html/images/img51.jpg
   :height: 400px

Dav Droid es una aplicación de pago. No obstante, existe un repositorio online de apps desarro-llado por el creador de Dav Droid donde podemos encontrarla de forma gratuita.
Dicho repositorio es F-Droid, y se puede instalar Dav Droid con él de la siguiente forma:
1.	Desde el navegador web, buscar F-Droid y acceder a la página oficial.

.. image:: ../../html/images/img52.jpg
   :scale: 50 %

.. image:: ../../html/images/img53.jpg
   :scale: 50 %

2.	Pulsar en el enlace para descargar. Cuando salga el cuadro de diálogo, pulsar en Abrir la carpeta, con lo que podremos visualizar la app en el directorio en el que se ha guardado:

.. image:: ../../html/images/img54.jpg
   :scale: 50 %

.. image:: ../../html/images/img55.jpg
   :scale: 50 %

3.	Seleccionar el archivo para instalar. Confirmar dicho proceso.

.. image:: ../../html/images/img56.jpg
   :scale: 50 %

.. image:: ../../html/images/img57.jpg
   :scale: 50 %

4.	Abrir F-Droid. Pulsar en el botón de búsqueda y escribir Dav Droid, con lo que saldrá di-cha aplicación disponible para su descarga.

.. image:: ../../html/images/img58.jpg
   :scale: 30 %

.. image:: ../../html/images/img59.jpg
   :scale: 30 %

.. image:: ../../html/images/img60.jpg
   :scale: 30 %

5.	Pulsar el botón de instalar. Confirmando en la siguiente pantalla pulsando de nuevo en instalar obtendremos Dav Droid de forma gratuita.

.. image:: ../../html/images/img61.jpg
   :scale: 50 %

.. image:: ../../html/images/img62.jpg
   :scale: 50 %

Otra alternativa es Open Sync, la cual funciona de manera similar y se puede instalar de la si-guiente forma:

1.	Instalar la aplicación Open Sync desde la Play Store.

.. image:: ../../html/images/img63.jpg
   :scale: 50 %

2.	Abrir la aplicación
La primera vez que se abre aparecerán dos diálogos: uno indicando la licencia y otro indi-cando una alternativa para sincronizar tareas (recomendamos seleccionar «Don’t show again».

.. image:: ../../html/images/img64.jpg
   :scale: 50 %

.. image:: ../../html/images/img65.jpg
   :scale: 50 %

3.	Añadir una nueva cuenta, pulsando en el botón + situado abajo a la derecha.
4.	Elegir «Login with URL and username».
5.	Indicar como dirección https://drive.upm.es
6.	Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios.
7.	Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdri-ve)
8.	Confirmar los datos seleccionando «Login»
9.	Elegir el nombre de la cuenta y confirmar seleccionando «Create Account»

.. image:: ../../html/images/img66.jpg
   :height: 400px

10.	En la pantalla de inicio, aparecerá la cuenta creada.
11.	Seleccionar la cuenta
12.	Elegir los calendarios y contactos se quieren sincronizar.
13.	Forzar una sincronización seleccionando el icono de refrescar en la parte superior dere-cha.

A partir de este momento, en el calendario y contactos aparecerán los de UPMdrive

.. image:: ../../html/images/img67.jpg
   :height: 400px


3.2.4	Dispositivos iOS
************************

iOS soporta nativamente tanto contactos como calendario.

3.2.4.1	Calendario
******************

1.	Abrir la aplicación de ajustes
2.	Seleccionar Calendario
3.	Seleccionar Cuentas
4.	Seleccionar Añadir cuenta
5.	Seleccionar Otra en el tiempo de cuenta
6.	Seleccionar Añadir cuenta CalDAV

.. image:: ../../html/images/img75.jpg
   :align: center
   

7.	En el servidor escribir https://drive.upm.es/remote.php/dav/principals/users/[USERNAME]/


La URL se puede copiar desde la parte inferior de la interfaz web.

.. image:: ../../html/images/img68.jpg
   :height: 400px
.. image:: ../../html/images/img69.jpg   
   :height: 400px

8.	Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios.
9.	Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdri-ve)
10.	Seleccionar Siguiente

El dispositivo deberá validar la configuración y mostrar los calendarios en la aplicación Calenda-rio.

3.2.4.2	Contactos
*****************

1.	Abrir la aplicación de ajustes
2.	Seleccionar Contactos
3.	Seleccionar Cuentas
4.	Seleccionar Añadir cuenta
5.	Seleccionar Otra en el tiempo de cuenta
6.	Seleccionar Añadir cuenta CardDAV

.. image:: ../../html/images/img70.jpg
   :align: center

7.	En el servidor escribir https://drive.upm.es/remote.php/dav/principals/users/[USERNAME]/

La URL se puede copiar desde la parte inferior de la interfaz web.

.. image:: ../../html/images/img71.jpg
   :height: 400px

.. image:: ../../html/images/img72.jpg
   :height: 400px

8.	Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios.
9.	Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdri-ve)
10.	Seleccionar Siguiente

El terminal deberá validar la configuración y mostrar los calendarios en la aplicación Contactos.

