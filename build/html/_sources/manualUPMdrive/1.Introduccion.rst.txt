===============
1. Introducción
===============
.. Cursiva

*¡Bienvenido a UPMdrive!*

A inicios de 2014 el Centro de Supercomputación y Visualización de Madrid (CeSViMa) lanza un almacenamiento en la nube para los investigadores de la Universidad Politécnica de Madrid (UPM) denominado «Nube UPM».

En septiembre de 2016, CeSViMa pasa a depender del Vicerrectorado de Servicios Tecnológicos de la UPM. La Nube UPM se convierte en el servicio UPMdrive, abriéndose a todo el personal de la Universidad.

Este manual de usuario pretende describir el servicio UPMdrive y proporcionar la guía para su configuración e instalación en los distintos dispositivos.


.. Para subsubtitulo

1.1 A quién va dirigido este manual
-----------------------------------

Este manual está dirigido al personal de la UPM que desee utilizar el servicio UPMdrive.

Se necesitan conocimientos a nivel de usuario del uso de ficheros en el ordenador.

1.2 Dónde encontrar más documentación
-------------------------------------

Este manual sólo pretende ser una pequeña guía para los usuarios que deseen utilizar UPMdrive. La última versión de la documentación estará en la web del `CeSViMa <http://www.cesvima.upm.es/>`_, dentro del portal de `documentación <http://docs.cesvima.upm.es/>`_. En el portal existen una serie de video–tutoriales que muestran los usos principales.

Además de esta guía específica, para una información genérica más completa, se puede consultar el manual oficial de `ownCloud <http://doc.owncloud.org/server/9.1/user_manual/>`_, el manual oficial del cliente de `escritorio <http://doc.owncloud.org/desktop/>`_  o los manuales para las aplicaciones `Android <http://doc.owncloud.org/android/>`_  e `iOS <http://doc.owncloud.org/ios/>`_ .

1.3 Avisos y notificaciones
---------------------------

Todas las direcciones de correo electrónico utilizadas para acceder al servicio son añadidas a las listas de distribución. Estas listas son utilizadas únicamente para proporcionar información a los usuarios: anuncios de novedades, incidencias del servicio, etc.

Para evitar la proliferación de correo no deseado, la lista está cerrada de tal forma que únicamente el personal de soporte pueda enviar correos a la misma.

Asimismo, al finalizar la prestación del servicio, la dirección es eliminada automáticamente de la lista.

1.4 Contactar con el soporte
----------------------------

Para solucionar cualquier duda o problema que surja con la configuración o uso de UPMdrive se puede contactar por correo electrónico con el Centro de Atención a Usuarios (`CAU <support@cesvima.upm.es>`_).
Siempre que sea posible, al contactar con el servicio de soporte se debe indicar de la forma más clara:

* Descripción del problema surgido o duda a resolver.

* Identificación del usuario, sobre todo si se utiliza una cuenta diferente de la que se utilizó en el registro.

* Fecha y hora aproximada en la que se detectó el problema o incidencia.

* Condiciones en las que se produce.

* Mensajes de error o información mostrados por el sistema en el momento de la incidencia, si se dispone de ellos.

Cada una de las comunicaciones iniciadas queda automáticamente registrada en el sistema de control de incidencias bajo un identificador único y conservándose el histórico.

