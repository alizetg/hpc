******************
2. Sobre esta guía
******************

A finales del año 2004, la Universidad Politécnica de Madrid aunó esfuerzos con IBM y el Centro Nacional de Supercomputación para crear el Centro de Supercomputación y Visualización de Madrid (CeSViMa). El CeSViMa, situado el Campus de Excelencia Internacional de Montegancedo una de las sedes del Parque Científico-Tecnológico de la UPM, se centra en el almacenamiento masivo de información, computación de altas prestaciones, visualización interactiva avanzada, virtualización y computación en la nube.

Dentro del área de computación de altas prestaciones oferta recursos mediante el supercomputador Magerit. La segunda versión de este supercomputador se convirtió en el más potente de España en la lista TOP500 de junio de 2011 [#f1]_ . También se posicionó como el más ecológico de España y uno de los más ecológicos del planeta [#f2]_

.. figure:: ../../html/images/img76.jpg
   :align: center
   :scale: 50 %
   :alt: alternate text

   La lista Green500 emite un certificado a cada centro indicando su posición en la misma. Gracias a la segunda versión de Magerit, el CeSViMa alcanzó la posición 18 en Junio de 2011"

Esta guía del usuario de Magerit proporciona una visión global de los sistemas y de los elementos que lo componen. También se describen los recursos y servicios disponibles destinados a supercomputación y cómo utilizarlos.


2.1 Audiencia
#############

Esta guía está dirigida a los usuarios que tienen algún tipo de relación con el supercomputador Magerit.

Aunque se pretende minimizar los conocimientos previos necesarios, se requieren conocimientos del sistema operativo Unix/Linux.

2.2 Guía de lectura
###################

Dependiendo del perfil de usuario, existen partes de este manual que tendrán mayor o menor relevancia:

 * **Nuevos usuarios.**

 Tienen una información general del sistema y sus condiciones de uso en los capítulos *Visión general* y *Condiciones de uso*. También disponen de una guía de iniciación básica en el capítulo *Primeros pasos*. A los usuarios de Windows les puede interesar el anexo   *SSH en Microsoft Windows* en el que se explica cómo configurar los clientes de acceso al sistema.

 * **Líderes de proyecto.**

 Si se encargan únicamente de la coordinación del proyecto y publicación de resultados, se pueden centrar en los capítulos *Visión general*, *Condiciones de uso* y *Servicios*.

 * **Investigadores y desarroladores.**

 Disponen de información general del sistema, y sus condiciones de uso en los capítulos *Visión general* y *Condiciones de uso*. En el capítulo *Ejecución de trabajos* se describe el protocolo para preparar y ejecutar trabajos en la máquina y, por último, en *Preguntas frecuentes (FAQ)* tiene información acerca de los problemas y dudas más consultados.

2.3 Última versión de la documentación
#######################################

Debido a las características del sistema, es muy difícil mantener la documentación constantemente actualizada. La información más completa es esta guía de usuarios cuya última versión estará disponible en la página web del CeSViMa.

Asimismo, la documentación más reciente, noticias, conferencias, seminarios y cualquier otra información de interés se encontrarán disponibles en la misma página web.

2.4 Contactar con el CeSViMa
############################

Si tras la lectura de la documentación proporcionada existen dudas o surgen problemas relacionados con el uso del supercomputador, se puede contactar con el Centro de atención a usuarios al correo  support@cesvima.upm.es.



.. [#f1] https://www.top500.org/list/2011/06/?page=2
.. [#f2] https://web.archive.org/web/20111121153545/http://www.green500.org:80/lists/2011/06/top/list.php?from=1&to=100